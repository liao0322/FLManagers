#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "FLAppManager.h"
#import "WXApiManager.h"
#import "XFAliPayManager.h"

FOUNDATION_EXPORT double FLManagersVersionNumber;
FOUNDATION_EXPORT const unsigned char FLManagersVersionString[];

