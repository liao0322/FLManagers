//
//  FLViewController.m
//  FLManagers
//
//  Created by liao0322 on 03/18/2018.
//  Copyright (c) 2018 liao0322. All rights reserved.
//

#import "FLViewController.h"
#import "FLAppManager.h"

@interface FLViewController ()

@end

@implementation FLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSLog(@"%@", [FLAppManager appVersion]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
