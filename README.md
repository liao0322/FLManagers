# FLManagers

[![CI Status](http://img.shields.io/travis/liao0322/FLManagers.svg?style=flat)](https://travis-ci.org/liao0322/FLManagers)
[![Version](https://img.shields.io/cocoapods/v/FLManagers.svg?style=flat)](http://cocoapods.org/pods/FLManagers)
[![License](https://img.shields.io/cocoapods/l/FLManagers.svg?style=flat)](http://cocoapods.org/pods/FLManagers)
[![Platform](https://img.shields.io/cocoapods/p/FLManagers.svg?style=flat)](http://cocoapods.org/pods/FLManagers)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FLManagers is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FLManagers'
```

## Author

liao0322, liao0322@hotmail.com

## License

FLManagers is available under the MIT license. See the LICENSE file for more info.
