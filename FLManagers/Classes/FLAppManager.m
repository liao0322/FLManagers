//
//  FLAppManager.m
//  FengLei
//
//  Created by liaoxf on 2017/11/25.
//  Copyright © 2017年 com.mlj.FengLei. All rights reserved.
//

#import "FLAppManager.h"

@implementation FLAppManager

+ (NSString *)appVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

@end
