//
//  XFAliPayManager.h
//  mddjb
//
//

#import <Foundation/Foundation.h>

@protocol XFAliPayManagerDelegate <NSObject>

@optional

- (void)managerDidRecvAliPayBackMessageRep:(id)response;


@end

@interface XFAliPayManager : NSObject

@property (nonatomic, weak) id<XFAliPayManagerDelegate> delegate;

+ (instancetype)sharedManager;

- (void)onResp:(id)rep;

@end
