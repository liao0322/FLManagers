//
//  FLAppManager.h
//  FengLei
//
//  Created by liaoxf on 2017/11/25.
//  Copyright © 2017年 com.mlj.FengLei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLAppManager : NSObject

+ (NSString *)appVersion;

@end
