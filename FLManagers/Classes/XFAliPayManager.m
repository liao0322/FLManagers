//
//  XFAliPayManager.m
//  mddjb
//
//

#import "XFAliPayManager.h"

@implementation XFAliPayManager

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static XFAliPayManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[XFAliPayManager alloc] init];
    });
    return instance;
}

- (void)onResp:(id)rep {
    if (self.delegate && [self.delegate respondsToSelector:@selector(managerDidRecvAliPayBackMessageRep:)]) {
        [self.delegate managerDidRecvAliPayBackMessageRep:rep];
    }
}


@end
